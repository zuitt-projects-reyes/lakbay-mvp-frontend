import React, { Fragment, useState, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

function AdminReservationView(props) {
  const { reservationData, fetchData } = props;
  const [reservations, setReservations] = useState([]);

  const fulfillReservation = (reservationId) => {

    fetch(`https://lakbay-server.herokuapp.com/admin/reservations/${reservationId}/fulfill`, {
      method: 'PATCH',
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${sessionStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {

        if (data) {
          fetchData();

          Swal.fire({
            title: "Success",
            icon: "success",
            text: "Reservation has been fulfilled"
          });

        } else {

          fetchData();

          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again."
          });

        }
      });
  };

  const cancelReservation = (reservationId) => {

    fetch(`https://lakbay-server.herokuapp.com/admin/reservations/${reservationId}/cancel`, {
      method: 'PATCH',
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${sessionStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {

        if (data) {
          fetchData();

          Swal.fire({
            title: "Success",
            icon: "success",
            text: "Reservation has been cancelled"
          });

        } else {
          fetchData();

          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please try again."
          });

        }
      });
  };


  useEffect(() => {
    const reservationsArr = reservationData.map((reservation) => {
      return (
        <tr key={reservation._id}>
          <td>{reservation.userId}</td>
          <td>{reservation.status}</td>
          <td>{reservation.reservations[0].name}</td>
          <td>{reservation.reservedAt}</td>
          <td>{reservation.totalPrice}</td>
          <td>
            {reservation.status === 'booked'
              ?
              <>
                <Button
                  variant="success"
                  size="sm"
                  className="ms-3"
                  onClick={() => fulfillReservation(reservation._id)}
                >
                  Fulfill
                </Button>

                <Button
                  variant="danger"
                  size="sm"
                  className="ms-3"
                  onClick={() => cancelReservation(reservation._id)}
                >
                  Cancel
                </Button>
              </>
              :
              <>
                <Button
                  variant="secondary"
                  disabled
                  size="sm"
                  className="ms-3"
                >
                  Fulfill
                </Button>

                <Button
                  variant="secondary"
                  disabled
                  size="sm"
                  className="ms-3"
                >
                  Cancel
                </Button>
              </>
            }
          </td>
        </tr>
      );
    });

    setReservations(reservationsArr);
  }, [reservationData, fetchData]);

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <Fragment>
      <div className="text-center my-4">
        <h2 className="fw-bold">Reservations Management</h2>
      </div>

      <Table striped bordered hover responsive>
        <thead className="bg-dark text-white">
          <tr>
            <th>User ID</th>
            <th>Status</th>
            <th>Reserved Room Name</th>
            <th>Reserved At</th>
            <th>Amount Paid</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>{reservations}</tbody>
      </Table>
    </Fragment>
  );
}

export default AdminReservationView;
