import React from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import '../pages/Home';
import '../css/Banner.css';

function Banner() {

	return (
		<div className="banner">

			<div className="banner_text">
				<h2>Dare to live the life you&apos;ve always dreamed of</h2>
				<h6 className="my-3">Plan your next getaway and discover the adventure that awaits.</h6>
				<Button variant="danger" className="btn btn-lg" as={Link} to={'/rooms'} >Explore Now</Button>
			</div>

		</div>
	);
}

export default Banner;