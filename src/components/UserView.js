import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import RoomCard from '../components/RoomCard';

function UserView({ roomData }) {

  const [rooms, setRooms] = useState([]);

  useEffect(() => {
    const roomsArr = roomData.map((room) => {
      if (room.isAvailable === true) {
        return (
          <Col xs={12} md={6} lg={4} key={room.id}>
            <RoomCard key={room._id} roomProps={room} />
          </Col>
        );
      } else {
        return null;
      }
    });

    setRooms(roomsArr);

  }, [roomData]);

  return (
    <Container>
      <h1 className="fw-bold mt-3 text-center">Book a Room Now!</h1>
      <Row className="mt-3 mb-3">
        {rooms.length > 0 ? rooms : <h2 className="text-center my-5 py-5">Loading...</h2>}
      </Row>
    </Container>
  );
}

export default UserView;
