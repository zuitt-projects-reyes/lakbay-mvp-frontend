import React, { Fragment, useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';

function AdminUsersView(props) {

  const { userData, fetchData } = props;
  const [users, setUsers] = useState([]);

  useEffect(() => {

    const usersArr = userData.map(user => {

      return (

        <tr key={user._id}>
          <td>{user._id}</td>
          <td>{user.firstName} {user.lastName}</td>
          <td>{user.phoneNum}</td>
          <td>{user.email}</td>
          <td>
            {user.isAdmin ? 'Admin' : 'User'}
          </td>
        </tr>
      );
    });

    setUsers(usersArr);
  }, [userData, fetchData]);

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <Fragment>

      <div className="text-center my-4">
        <h2 className="fw-bold">User Account Management</h2>
      </div>

      <Table striped bordered hover responsive>
        <thead className="bg-dark text-white">
          <tr>
            <th>User ID</th>
            <th>Name</th>
            <th>Phone Number</th>
            <th>Email</th>
            <th>Account Type</th>
          </tr>
        </thead>
        <tbody>
          {users}
        </tbody>
      </Table>

    </Fragment>
  );
}

export default AdminUsersView;
