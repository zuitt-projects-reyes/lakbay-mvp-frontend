import React from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

function RoomCard({ roomProps }) {

  const { name, shortAddress, price, _id } = roomProps;

  return (
    <Card>
      <Card.Body className="align-items-stretch">
        <Card.Title className="h1 pb-3 fw-bold">{name}</Card.Title>
        <Card.Subtitle>Address:</Card.Subtitle>
        <Card.Text>{shortAddress}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>
        <Card.Text>PHP {price}.00</Card.Text>
        <Button variant="danger" as={Link} to={`/rooms/${_id}`}>See More</Button>
      </Card.Body>
    </Card>
  );
}

export default RoomCard;