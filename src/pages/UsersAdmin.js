import React, { useState, useEffect, useContext } from 'react';
import { Container } from 'react-bootstrap';
import AdminView from '../components/AdminUserView.js';
import UserContext from '../UserContext';
import Error from './ErrorUnauthorized';

function UserAdmin() {

  const { user } = useContext(UserContext);
  const [users, setUsers] = useState([]);

  const fetchAdminUsers = async () => {
    try {
      const response = await fetch('https://lakbay-server.herokuapp.com/admin/users', {
        headers: {
          Authorization: `Bearer ${sessionStorage.getItem('token')}`
        }
      });
      const data = await response.json();
      setUsers(data);
    } catch (error) {
      console.log(`I JUST WANNA TELL YOU HOW I'M FEELING`);
    }
  };

  // Fetch data from database on first render
  useEffect(() => {
    if (user.id !== null && user.isAdmin) {
      fetchAdminUsers();
    }
  }, []);

  return (
    <Container className="p-4">
      {(user.isAdmin === true) ?
        <AdminView userData={users} fetchData={fetchAdminUsers} />
        :
        <Error />
      }
    </Container>
  );
}

export default UserAdmin;
