import React, { useState, useEffect, useContext } from 'react';
import { Container } from 'react-bootstrap';
import AdminView from './../components/AdminRoomView.js';
import UserView from './../components/UserView.js';
import UserContext from './../UserContext';

function Rooms() {

  const { user } = useContext(UserContext);
  const [rooms, setRooms] = useState([]);

  const fetchRooms = async () => {
    try {
      const response = await fetch('https://lakbay-server.herokuapp.com/rooms');
      const data = await response.json();
      setRooms(data);
    } catch (error) {
      console.log(`WE'RE NO STRANGERS TO LOVE`);
    }
  };

  const fetchAdminRooms = async () => {
    try {
      const response = await fetch('https://lakbay-server.herokuapp.com/admin/rooms', {
        headers: {
          Authorization: `Bearer ${sessionStorage.getItem('token')}`
        }
      });
      const data = await response.json();
      setRooms(data);
    } catch (error) {
      console.log('YOU KNOW THE RULES AND SO DO I');
    }
  };

  useEffect(() => {
    if (user.id !== null && user.isAdmin) {
      fetchAdminRooms();
    } else {
      fetchRooms();
    }

  }, []);

  return (
    <Container className="p-4">
      {(user.isAdmin === true) ?
        <AdminView roomData={rooms} fetchData={fetchAdminRooms} />

        :
        <UserView roomData={rooms} />
      }
    </Container>
  );
}

export default Rooms;
