import React from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import '../css/Error.css';

function ErrorNotFound() {
  return (
    <div className="center-me">
      <h1 className="fw-bold error-text">404 Not Found</h1>
      <h2 className="py-5 text-center">The page you are looking for does not exist.</h2>
      <Button className="btn btn-lg px-5 my-4 center-btn" variant="danger" as={Link} to={'/'}>Return</Button>
    </div>
  );
}

export default ErrorNotFound;
