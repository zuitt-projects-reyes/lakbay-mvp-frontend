import React, { Fragment, useState, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useNavigate, Link } from 'react-router-dom';

function Booking({ fetchReservations }) {

  const navigate = useNavigate();

  const [bookingData, setBookingData] = useState([]);
  const [reservations, setReservations] = useState([]);
  const [price, setPrice] = useState(0);

  const fetchBookings = async () => {
    try {
      const response = await fetch('https://lakbay-server.herokuapp.com/bookings', {
        headers: {
          Authorization: `Bearer ${sessionStorage.getItem('token')}`
        }
      });
      const data = await response.json();
      setBookingData(data.reservations);
      setPrice(data.totalPrice);
    } catch (error) {
      console.log('NEVER GONNA MAKE YOU CRY');
    }
  };

  // Function that clears the entire booking
  const clearBooking = async () => {
    try {
      const response = await fetch('https://lakbay-server.herokuapp.com/bookings/clear', {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${sessionStorage.getItem("token")}`
        }
      });
      if (!response.ok) {
        throw new Error(response.status);
      }
      Swal.fire({
        title: "Booking cleared!",
        icon: "success",
        text: "You have successfully cleared your booking!"
      });
      navigate('/profile');

    } catch (error) {
      Swal.fire({
        title: "Error",
        icon: "error",
        text: "Something went wrong!"
      });
    }
  };

  const confirmBooking = async () => {
    try {
      const response = await fetch('https://lakbay-server.herokuapp.com/bookings/confirm', {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${sessionStorage.getItem('token')}`
        }
      });
      if (!response.ok) {
        throw new Error(response.status);
      }

      Swal.fire(
        'Confirmed!',
        'Your booking has been confirmed. We hope you enjoy your stay!',
        'success'
      );
      fetchReservations();
      navigate('/profile');

    } catch (error) {
      console.log('NEVER GONNA SAY GOODBYE');
    }
  };


  useEffect(() => {

    fetchBookings();

    if (!bookingData || bookingData.length === 0) return;

    const bookingsArr = bookingData.map(reservation => {
      return (
        <tr key={reservation.roomId}>
          <td>{reservation.name}</td>
          <td>{reservation.shortAddress}</td>
          <td>PHP {reservation.subtotal}.00</td>
        </tr>
      );
    });
    setReservations(bookingsArr);

  }, [bookingData]);

  useEffect(() => {
    fetchBookings();
  }, []);

  return (
    <Fragment>

      <div className="text-center my-5">
        <h1 className="fw-bold">Your Booking</h1>
      </div>

      {!bookingData || bookingData.length === 0 ? (
        <div className="text-center my-4 py-4">
          <h3>No rooms booked yet, try checking out one of our<Link className="mx-2" to="/rooms" style={{ textDecoration: 'underline' }}>
            rooms
          </Link>first!
          </h3>
        </div>
      ) : (
        <>
          <Table striped bordered hover responsive>
            <thead className="bg-dark text-white">
              <tr>
                <th>Name</th>
                <th>Short Address</th>
                <th>Subtotal</th>
              </tr>
            </thead>
            <tbody>
              {reservations}
            </tbody>
          </Table>
          <h4 className="mt-3 fw-bold" style={{ textAlign: 'right' }}>Total: PHP {price}.00</h4>

          {/* Button that when clicked, clears the booking */}
          <Button
            variant="danger"
            className="mb-5"
            onClick={() => {
              Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, clear it!'
              }).then((result) => {
                if (result.value) {
                  clearBooking();
                }
              });
            }}
          >Clear Booking</Button>

          <Button variant="primary" className="ms-3 mb-5" onClick={() => {
            Swal.fire({
              title: 'Are you sure?',
              text: `You will be paying a total of PHP ${price}.00 for this booking.`,
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, confirm it!'
            }).then((result) => {
              if (result.value) {
                confirmBooking();
              }
            });
          }}>Confirm Booking</Button>
        </>

      )
      }

    </Fragment >
  );
}

export default Booking;