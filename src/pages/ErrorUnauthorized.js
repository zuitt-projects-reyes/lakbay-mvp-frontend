import React from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import '../css/Error.css';

function ErrorUnauthorized() {
  return (
    <div className="center-me">
      <h1 className="fw-bold error-text">401 Unauthorized</h1>
      <h2 className="py-5 text-center">You shall not pass!</h2>
      <Button className="btn btn-lg px-5 my-4 center-btn" variant="danger" as={Link} to={'/'}>Return</Button>
    </div>
  );
}

export default ErrorUnauthorized;