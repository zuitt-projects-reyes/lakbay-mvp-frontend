import React, { useState, useEffect, useContext } from 'react';
import { Container } from 'react-bootstrap';
import AdminView from '../components/AdminReservationView.js';
import UserContext from '../UserContext';
import Error from './ErrorUnauthorized';

function ReservationsAdmin() {

  const { user } = useContext(UserContext);
  const [reservations, setReservations] = useState([]);

  const fetchAdminReservations = async () => {
    try {
      const response = await fetch('https://lakbay-server.herokuapp.com/admin/reservations', {
        headers: {
          Authorization: `Bearer ${sessionStorage.getItem('token')}`
        }
      });
      const data = await response.json();
      setReservations(data);
    } catch (error) {
      console.log('NEVER GONNA TELL A LIE AND HURT YOU');
    }
  };

  // Fetch data from database
  useEffect(() => {
    if (user.id !== null && user.isAdmin) {
      fetchAdminReservations();
    }
  }, []);

  return (
    <Container className="p-4">
      {(user.isAdmin === true) ?
        <AdminView reservationData={reservations} fetchData={fetchAdminReservations} />
        :
        <Error />
      }
    </Container>
  );
}

export default ReservationsAdmin;
