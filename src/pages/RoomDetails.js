import React, { useState, useEffect, useContext } from 'react';
import { Container, Button } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import samplePic from '../img/sample-room-pic.webp';

function RoomDetails() {

  const { user } = useContext(UserContext);
  const history = useNavigate();

  const [name, setName] = useState('');
  const [address, setAddress] = useState('');
  const [price, setPrice] = useState(0);

  const { roomId } = useParams();

  window.scrollTo(0, 0);

  useEffect(() => {

    fetch(`https://lakbay-server.herokuapp.com/rooms/${roomId}`)
      .then(res => res.json())
      .then(data => {
        setName(data.name);
        setAddress(data.shortAddress);
        setPrice(data.price);
      });
  }, [roomId]);

  const addToCart = async () => {

    try {
      const response = await fetch('https://lakbay-server.herokuapp.com/bookings/add', {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${sessionStorage.getItem("token")}`
        },
        body: JSON.stringify({
          roomId: roomId
        })
      });
      if (!response.ok) {
        throw new Error(response.status);
      }

      Swal.fire({
        title: "Room booked!",
        icon: "success",
        text: "You have successfully added this room to your booking!"
      });

      history("/bookings");
    } catch (error) {

      Swal.fire({
        title: "Booking is Full",
        icon: "error",
        text: "Please clear your booking first"
      });

    }
  };

  let checkoutBtn;

  if (user.id !== null && user.isAdmin === true) {
    checkoutBtn = <Link className="btn btn-lg btn-secondary" to="/login">Log in as User to Book</Link>;
  } else if (user.id !== null) {
    checkoutBtn = <Button className="btn btn-lg" variant="danger" onClick={addToCart}>Book this Room</Button>;
  } else {
    checkoutBtn = <Link className="btn btn-lg btn-warning" to="/login">Log In to Book</Link>;
  }

  return (
    // if there is no data, show loading page
    name === '' ? <h2 className="text-center my-5 py-5">Loading...</h2> :
      <Container >
        <div className="mt-5 mb-4 d-flex">
          <img src={samplePic} alt="sample room" height="300px" />
          <div className="ms-4">
            <h1 className="fw-bold mt-3 mb-4">{name}</h1>
            <h4 className="mb-4">Address: {address}</h4>
            <h4 className="mb-5">Price: PHP {price}.00</h4>
            {checkoutBtn}
          </div>
        </div>
        <h3 className="my-4 fw-bold">Description</h3>
        <p>
          Enjoy our elegant 40 m² guest rooms, designed in warm beige tones and tailored to the needs of private and business travelers alike. All rooms have a large marble bathroom, a double bed, air conditioning, an additional work area with free internet access and a walk-in closet. Our superior rooms impress with a wonderful view of the city and the adjacent park.
        </p>
        <p>
          You’ll notice it the instant you walk in – the attention to detail and the effort we made to meet your needs and requirements. Starting from the comfortable king-size bed to the spacious walk-in closet to the large marble bathroom with separate shower and bathtub. 24 hour room service, bathrobe and slippers and, upon your request, the daily newspaper at your door – we don’t want you to lack anything.
        </p>
        <hr />
        <h3 className="my-4 fw-bold">Amenities</h3>
        <ul>
          <li className="h5">Air conditioning</li>
          <li className="h5">Free Wi-Fi</li>
          <li className="h5">Free Parking</li>
          <li className="h5">Free Breakfast</li>
          <li className="h5">Free Laundry Service</li>
          <li className="h5">Free Toiletries</li>
          <li className="h5">Nearby Fitness Center</li>
        </ul>
        <hr />
        <h3 className="my-4 fw-bold">Reviews</h3>
        <h5 className="mt-2 mb-5 pb-5">Coming soon...</h5>
      </Container>
  );
}

export default RoomDetails;