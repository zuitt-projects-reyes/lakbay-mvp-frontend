import React, { Fragment, useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import defaultPic from '../img/default-pic.png';

function Profile(props) {

  const { profileData, fetchProfile, reservationData, fetchReservations } = props;

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [address, setAddress] = useState('');
  const [phoneNum, setPhoneNum] = useState('');
  const [email, setEmail] = useState('');

  const [reservations, setReservations] = useState([]);

  useEffect(() => {
    setFirstName(profileData.firstName);
    setLastName(profileData.lastName);
    setAddress(profileData.address);
    setPhoneNum(profileData.phoneNum);
    setEmail(profileData.email);
  }, [profileData]);

  useEffect(() => {
    fetchProfile();
    fetchReservations();

    if (!reservationData) return;

    const reservationsArr = reservationData.map(reservation => {
      return (
        <tr key={reservation._id}>
          <td>{reservation.reservations[0].name}</td>
          <td>{reservation.status}</td>
          <td>{reservation.reservedAt}</td>
          <td>PHP {reservation.totalPrice}.00</td>
        </tr>
      );
    });

    setReservations(reservationsArr);
  }, [reservationData, fetchReservations]);

  return (
    (Object.keys(profileData).length === 0 ? <h2 className="text-center my-5 py-5">Loading...</h2> :
      <>
        <h1 className="p-3 mt-4 text-center fw-bold">My Profile</h1>
        <div className="d-flex">
          <img
            className="me-5 my-2"
            src={defaultPic}
            alt="Lakbay Logo"
            height="200px"
            width="200px"
          ></img>
          <div className="mt-2">
            <h4 className="pt-3">Full Name: {firstName} {lastName}</h4>
            <h4>Address: {address}</h4>
            <h4>Phone Number: {phoneNum}</h4>
            <h4>Email Address: {email}</h4>
          </div>
        </div>
        <hr />
        <h1 className="p-3 text-center fw-bold">My Reservations</h1>
        {reservationData.length === 0 ? <h2 className="text-center my-5 py-5">Loading...</h2>
          :
          <Table striped bordered hover className="mb-5">
            <thead>
              <tr>
                <th>Name</th>
                <th>Status</th>
                <th>Date Reserved</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              {reservations}
            </tbody>
          </Table>
        }


      </>)
  );
}

export default Profile;
