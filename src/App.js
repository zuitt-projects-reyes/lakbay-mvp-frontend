import React, { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import AppNavbar from './components/AppNavbar';
import Footer from './components/Footer';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Rooms from './pages/Rooms';
import RoomDetails from './pages/RoomDetails';
import UsersAdmin from './pages/UsersAdmin';
import ReservationsAdmin from './pages/ReservationsAdmin';
import Booking from './pages/Bookings';
import Profile from './pages/Profile';
import Error from './pages/ErrorNotFound';
import { UserProvider } from './UserContext';

function App() {

  // Define user session state based on token
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const [profileData, setProfileData] = useState({});
  const [reservationData, setReservationData] = useState([]);
  const fetchProfile = async () => {
    try {
      const response = await fetch('https://lakbay-server.herokuapp.com/users/profile', {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${sessionStorage.getItem('token')}`
        }
      });
      const data = await response.json();
      setProfileData(data);

    } catch (error) {
      console.log('NEVER GONNA GIVE YOU UP');
    }
  };

  // Fetch the user's reservations
  const fetchReservations = async () => {
    try {
      const response = await fetch('https://lakbay-server.herokuapp.com/users/profile/reservations', {
        headers: {
          Authorization: `Bearer ${sessionStorage.getItem('token')}`
        }
      });
      const data = await response.json();
      setReservationData(data);

    } catch (error) {
      console.log('NEVER GONNA LET YOU DOWN');
    }
  };

  // Fetch JWT from sessionStorage and verify using endpoint
  useEffect(() => {

    const verifyUser = async () => {
      const token = sessionStorage.getItem('token');
      if (!token) return;

      try {
        const response = await fetch('https://lakbay-server.herokuapp.com/users/verify', {
          method: 'GET',
          headers: { 'Authorization': `Bearer ${token}` }
        });

        if (!response.ok) return;

        const data = await response.json();
        setUser({ id: data.id, isAdmin: data.isAdmin });

      } catch (error) {
        console.log('NEVER GONNA RUN AROUND AND DESERT YOU');
      }
    };
    verifyUser();
    fetchProfile();
    fetchReservations();
  }, []);

  return (
    <UserProvider value={{ user, setUser }}>
      <BrowserRouter>
        <AppNavbar />
        <Container>
          <Routes>
            <Route exact path="/" element={<Home />} />

            <Route exact path="/rooms" element={<Rooms />} />
            <Route exact path="/rooms/:roomId" element={<RoomDetails />} />
            <Route exact path="/bookings" element={<Booking
              fetchReservations={fetchReservations} />} />

            <Route exact path="/admin/users" element={<UsersAdmin />} />
            <Route exact path="/admin/reservations" element={<ReservationsAdmin />} />

            <Route exact path="/profile" element={<Profile
              profileData={profileData}
              fetchProfile={fetchProfile}
              reservationData={reservationData}
              fetchReservations={fetchReservations} />} />

            <Route exact path="/register" element={<Register />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/logout" element={<Logout />} />

            <Route exact path="*" element={<Error />} />
          </Routes>
        </Container>
        <Footer />
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
